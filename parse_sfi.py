#!/usr/bin/env python3

import struct
from collections import namedtuple

SFI_NAME_LEN = 16

sfi_table_header = namedtuple('sfi_table_header', 'signature length revision checksum oem_id oem_table_id')
sfi_mem_entry = namedtuple('sfi_mem_entry', 'type phys_start virt_start pages attrib')
sfi_cpu_table_entry = namedtuple('sfi_cpu_table_entry', 'apic_id')
sfi_cstate_table_entry = namedtuple('sfi_cstate_table_entry', 'hint latency')
sfi_apic_table_entry = namedtuple('sfi_apic_table_entry', 'phys_addr')
sfi_freq_table_entry = namedtuple('sfi_freq_table_entry', 'freq_mhz latency ctrl_val')
sfi_wake_table_entry = namedtuple('sfi_wake_table_entry', 'phys_addr')
sfi_timer_table_entry = namedtuple('sfi_timer_table_entry', 'phys_addr freq_hz irq')
sfi_rtc_table_entry = namedtuple('sfi_rtc_table_entry', 'phys_addr irq')
sfi_device_table_entry = namedtuple('sfi_device_table_entry', 'type host_num addr irq max_freq name')
sfi_gpio_table_entry = namedtuple('sfi_gpio_table_entry', 'controller_name pin_no pin_name')

fmts = {
    "sfi_table_header": f"=4s I B B 6s 8s",
    "sfi_mem_entry": f"=I Q Q Q Q",
    "sfi_cpu_table_entry": f"=I",
    "sfi_cstate_table_entry": f"=I I",
    "sfi_apic_table_entry": f"=Q",
    "sfi_freq_table_entry": f"=I I I",
    "sfi_wake_table_entry": f"=Q",
    "sfi_timer_table_entry": f"=Q I I",
    "sfi_rtc_table_entry": f"=Q I",
    "sfi_device_table_entry": f"=B B H B I {SFI_NAME_LEN}s",
    "sfi_gpio_table_entry": f"={SFI_NAME_LEN}s H {SFI_NAME_LEN}s",
}

def parse_header(f):
    fmt = fmts[sfi_table_header.__name__]
    header = sfi_table_header._make(struct.unpack(fmt, f.read(struct.calcsize(fmt))))
    print(header)

def parse_table(path, tuple):
    fmt = fmts[tuple.__name__]
    with open("/sys/firmware/sfi/tables/" + path, "rb") as f:
        parse_header(f)
        while bytes := f.read(struct.calcsize(fmt)):
            entry = tuple._make(struct.unpack(fmt, bytes))
            print(" ", entry)


parse_table("MMAP", sfi_mem_entry)
parse_table("CPUS", sfi_cpu_table_entry)
parse_table("APIC", sfi_apic_table_entry)
parse_table("FREQ", sfi_freq_table_entry)
parse_table("MCFG", sfi_mem_entry)
parse_table("MRTC", sfi_rtc_table_entry)
parse_table("MTMR", sfi_timer_table_entry)
parse_table("SYST", sfi_timer_table_entry)
parse_table("WAKE", sfi_wake_table_entry)
parse_table("DEVS", sfi_device_table_entry)
parse_table("GPIO", sfi_gpio_table_entry)
